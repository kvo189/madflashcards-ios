//
//  FlashcardSetDetailViewController.swift
//  flashcards
//
//  Created by Khang Vo on 2/12/21.
//

import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet var tableView: UITableView!
    let reuseIdentifier = "CardCell"
    var cards: [Flashcard] = [Flashcard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Flashcard Set Detail"
        //let cardClass = Flashcard()
        cards = Flashcard.getHardCodedCollection()
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(FlashcardSetDetailViewController.handleLongPress))
        tableView.addGestureRecognizer(longPress)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) 
        
        cell.textLabel?.text = cards[indexPath.row].term
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let alertController = UIAlertController(title: cards[index].term, message: cards[index].definition, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Edit", style: .default, handler: {
            action in self.editableAlert(cardIndex: index)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func editableAlert(cardIndex: Int){
        var termTextField: UITextField = UITextField()
        var defTextField: UITextField = UITextField()
        let alertController = UIAlertController(title: "Edit Flashcard", message: nil, preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: {(txtTerm) -> Void in
            txtTerm.text = String(self.cards[cardIndex].term)
            termTextField = txtTerm
        })
        
        alertController.addTextField(configurationHandler: {(txtDef) -> Void in
            txtDef.text = String(self.cards[cardIndex].definition)
            defTextField = txtDef
        })
        
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: {action in
            self.updateItem(item: Flashcard(term: termTextField.text ?? "", definition: defTextField.text ?? ""), position: cardIndex)
        }))
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addFlashCard(_ sender: UIButton) {
        let newCard = Flashcard(term: "New Card", definition: "New def")
        self.cards.append(newCard)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [IndexPath.init(row: self.cards.count-1, section: 0)], with: .automatic)
        self.tableView.endUpdates()
    }
    @IBAction func studyFlashcard(_ sender: UIButton) {
        //performSegue(withIdentifier: "GoToStudy", sender: self)
    }
    
    func updateItem(item: Flashcard, position: Int) {
        cards[position] = item
        tableView.reloadRows(at: [IndexPath(row: position, section: 0)], with: .automatic)
    }
    
    @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let touchPt = sender.location(in: tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPt){
                editableAlert(cardIndex: indexPath.row)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
