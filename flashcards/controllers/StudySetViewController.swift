//
//  StudySetViewController.swift
//  flashcards
//
//  Created by Khang Vo on 2/17/21.
//

import UIKit

class StudySetViewController: UIViewController {
    private lazy var cards: [Flashcard] = [Flashcard]()
    private lazy var cardsMissed: [Bool] = [Bool]()
    private var totalCards: Int = 0
    private var missed: Int = 0
    private var correct: Int = 0
    private var completed: Int = 0
    
    @IBOutlet var buttonFlashcard: UIButton!
    @IBOutlet var completedLabel: UILabel!
    @IBOutlet var missedLabel: UILabel!
    @IBOutlet var correctLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cards = Flashcard.getHardCodedCollection()
        // Do any additional setup after loading the view.
        cardsMissed = Array(repeating: false, count: cards.count)
        totalCards = cards.count
        
        if (cards.isEmpty) {
            buttonFlashcard.setTitle("Flashcard Set is empty", for: .normal)
        }else{
            buttonFlashcard.setTitle(cards[0].term, for: .normal)
        }
        completedLabel.text = "Completed: \(completed) / \(totalCards)"
        correctLabel.text = "Correct: \(correct)"
        missedLabel.text = "Missed: \(missed)"
        
    }
    
    @IBAction func buttonFlashcardClick(_ sender: Any) {
        flipFlashcard()
    }
    @IBAction func buttonMissedClick(_ sender: Any) {
        markIncorrect()
    }
    @IBAction func buttonSkipClick(_ sender: Any) {
        skip()
    }
    @IBAction func buttonCorrectClick(_ sender: Any) {
        markCorrect()
    }
    
    private func flipFlashcard() {
        if (cards.isEmpty) {return}
        if (buttonFlashcard.currentTitle == cards[0].term){
            buttonFlashcard.setTitle(cards[0].definition, for: .normal)
        }else{
            buttonFlashcard.setTitle(cards[0].term, for: .normal)
        }
    }
    
    private func skip(){
        if (!cards.isEmpty) {
            cards.append(cards.removeFirst())
            cardsMissed.append(cardsMissed.removeFirst())
            buttonFlashcard.setTitle(cards[0].term, for: .normal)
        }
    }
    
    private func markIncorrect(){
        if (!cards.isEmpty) {
            cards.append(cards.removeFirst())
            cardsMissed[0] = true
            cardsMissed.append(cardsMissed.removeFirst())
            buttonFlashcard.setTitle(cards[0].term, for: .normal)
            missed += 1
            missedLabel.text = "Missed: \(missed)"
        }
    }
    
    private func markCorrect() {
        if (!cards.isEmpty) {
            if (!cardsMissed[0]){
                correct+=1
                correctLabel.text = "Correct: \(correct)"
            }
        
            completed+=1
            completedLabel.text = "Completed: \(completed) / \(totalCards)"
            
            cards.removeFirst()
            cardsMissed.removeFirst()
            
            if (cards.isEmpty) {
                buttonFlashcard.setTitle("Flashcard Set is empty", for: .normal)
            }else{
                buttonFlashcard.setTitle(cards[0].term, for: .normal)
            }
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
