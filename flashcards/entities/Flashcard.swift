//
//  Flashcard.swift
//  flashcards
//
//  Created by Khang Vo on 2/10/21.
//

import Foundation

class Flashcard {
    var term: String = ""
    var definition: String = ""
    
    init(term: String, definition: String) {
        self.term = term
        self.definition = definition
    }
    
    class func getHardCodedCollection() -> [Flashcard]
    {
        var flashcards = [Flashcard]()
        for i in 1...10
        {
            let flashcard = Flashcard(term: "Term \(i)", definition: "Definition \(i)")

            flashcards.append(flashcard)
        }
        return flashcards
    }
}


