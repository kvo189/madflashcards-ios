//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Khang Vo on 2/10/21.
//

import Foundation

class FlashcardSet {
    var title: String = ""
    
    init(title: String) {
        self.title = title
    }
    
    class func getHardCodedCollection() -> [FlashcardSet]
    {
        var sets = [FlashcardSet]()
        for i in 1...10
        {
            let set = FlashcardSet(title: "Title \(i)")
            sets.append(set)
        }
        return sets
    }
}
